﻿using System.Reflection;

[assembly: AssemblyVersion("0.3.1.0")]
[assembly: AssemblyFileVersion("0.3.1.0")]
[assembly: AssemblyInformationalVersion("0.3.1.0 Beta")] // The current format is the full version number and the development stage separated by space.