﻿namespace Client.Wpf.Enumerations.ShrinkProfiles
{
    public enum EVehicleProfile
    {
        None,
        Nation,
        NationAndCountry,
    }
}