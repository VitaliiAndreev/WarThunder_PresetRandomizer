﻿namespace Core.DataBase.WarThunder.Enumerations
{
    /// <summary> Nations available in War Thunder. </summary>
    public enum ENation
    {
        None = -1,
        All,
        Usa,
        Germany,
        Ussr,
        Britain,
        Japan,
        China,
        Italy,
        France,
        Sweden,
    }
}